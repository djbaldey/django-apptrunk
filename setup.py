#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
"""
Данный установочный файл не является полноценным установщиком и не решает всех
зависимостей, а предназначен лишь для создания ссылки в виртуальном окружении
на локальный git-репозиторий, например так:

    pip install -e ~/src/django-apptrunk/

"""
from setuptools import setup, find_packages
import apptrunk

setup(
    name='AppTrunk',
    version=apptrunk.__version__,
    description=(
        'A Django-service for storing and deploying ready-made '
        '(assembled) applications with restricted access to versions.'
    ),
    author='Grigoriy Kramarenko',
    author_email='root@rosix.ru',
    url='https://gitlab.com/djbaldey/django-apptrunk',
    platforms='any',
    zip_safe=False,
    packages=find_packages(),
    include_package_data=True,
    install_requires=[],
)
