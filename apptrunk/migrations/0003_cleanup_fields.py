# Generated by Django 3.2.7 on 2021-09-25 00:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apptrunk', '0002_version_semver'),
    ]

    operations = [
        migrations.AddField(
            model_name='branch',
            name='cleanup',
            field=models.PositiveSmallIntegerField(blank=True, help_text='Установите значение оставляемых после автоматической чистки минорных версий. Пустое и нулевое значения отключают такую чистку.', null=True, verbose_name='подчищать'),
        ),
        migrations.AddField(
            model_name='project',
            name='cleanup',
            field=models.PositiveSmallIntegerField(blank=True, help_text='Это значение означает, что все новые ветки будут созданы с этим значением очистки. Так же это значение применится для всех существующих веток с пустым значением очистки.', null=True, verbose_name='очистка для новых веток'),
        ),
    ]
