#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
"""
Functions for access control.
"""
import logging
from base64 import b64decode
from functools import wraps

from django.http import HttpResponse
from django.utils.timezone import now

from apptrunk.models import Client
from apptrunk.conf import (
    SYSTEM_AUTH_BASIC, SYSTEM_AUTH_API_KEYS, SYSTEM_AUTH_API_TOKENS,
)

logger = logging.getLogger('apptrunk.access')
log_debug = logger.debug
log_warn = logger.warning


def parse_auth(auth, intid=False):
    data = {}
    try:
        token_type, x, credentials = auth.partition(' ')
        if token_type == 'ApiKey':
            if ':' in credentials:
                cid, api_key = credentials.split(':', 1)
                if intid:
                    cid = int(cid)
                data = {'id': cid, 'api_key': api_key}
            else:
                data = {'token': credentials}
                token_type += 'Token'
        elif token_type == 'Basic':
            username, password = b64decode(credentials).decode().split(':')
            data = {'username': username, 'password': password}
        data['type'] = token_type
    except Exception as e:
        logger.error('Authorization parsing error.', exc_info=e)
    else:
        log_debug('Authorization by %s.', token_type)
    return data


def client_auth(request):
    auth = request.headers.get('Authorization')
    client = None
    if auth:
        qs = Client.objects.filter(is_active=True)
        credentials = parse_auth(auth, intid=True)
        if 'type' not in credentials:
            pass
        elif credentials['type'] == 'Basic':
            client = qs.filter(
                clientauth__username=credentials['username'],
                clientauth__password=credentials['password'],
            ).first()
        elif credentials['type'] == 'ApiKey':
            client = qs.filter(
                id=credentials['id'],
                api_key=credentials['api_key'],
            ).first()
        elif credentials['type'] == 'ApiKeyToken':
            client = qs.filter(
                clienttoken__code=credentials['token'],
                clienttoken__expired__gte=now(),
            ).first()
    return client


def system_auth(request):
    system = None
    auth = request.headers.get('Authorization')
    if auth:
        credentials = parse_auth(auth)
        if 'type' not in credentials:
            pass
        elif credentials['type'] == 'Basic':
            username = credentials['username']
            password = credentials['password']
            system = SYSTEM_AUTH_BASIC.get(username) == password
        elif credentials['type'] == 'ApiKey':
            id = credentials['id']
            api_key = credentials['api_key']
            system = SYSTEM_AUTH_API_KEYS.get(id) == api_key
        elif credentials['type'] == 'ApiKeyToken':
            system = credentials['token'] in SYSTEM_AUTH_API_TOKENS
        request.system_credentials = credentials
    return system


def client_access_required(function=None, raise_401=True, or_system=False):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            # Пользователь с разрешением на просмотр всех версий.
            if request.user.has_perm('apptrunk.view_version'):
                request.client = None
                return view_func(request, *args, **kwargs)
            system = client = None
            if or_system:
                system = system_auth(request)
            if not system:
                client = client_auth(request)
            else:
                client = None

            if raise_401 and not (client or system):
                response = HttpResponse(
                    'You need to authenticate.',
                    status=401,
                )
                response['WWW-Authenticate'] = 'Basic realm="Client Access"'
                return response

            request.client = client
            return view_func(request, *args, **kwargs)
        return _wrapped_view
    if function:
        return decorator(function)
    return decorator


def system_access_required(function=None):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            system = system_auth(request)

            if not system:
                response = HttpResponse(
                    'You need to authenticate.',
                    status=401,
                )
                response['WWW-Authenticate'] = 'Basic realm="System Access"'
                return response

            return view_func(request, *args, **kwargs)
        return _wrapped_view
    if function:
        return decorator(function)
    return decorator
