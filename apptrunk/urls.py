#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.urls import path
from apptrunk.views import index, versions, upload, check, download

app_name = 'apptrunk'

urlpatterns = [
    path('', index, name='index'),
    path('upload', upload, name='upload'),
    path('upload/', upload, name='upload_with_slash'),
    path('versions/<str:project_name>/<path:branch_name>', versions, name='versions'),
    path('check/<str:project_name>/<path:branch_name>', check, name='check'),
    path('download/<str:project_name>/<path:branch_name>', download, name='download'),
]
