#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from base64 import b64encode
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, Client as Browser
from django.urls import reverse
from django.utils.crypto import get_random_string
from django.utils.timezone import now, timedelta

from apptrunk import models, conf, forms


class UploadFormTestCase(TestCase):

    def tearDown(self):
        for v in models.Version.objects.all():
            v.delete()

    def test_suffix_no_valid(self):
        broken = (
            '-alpha/1',
            '-alpha\\1',
            '-alpha\\1',
            '-alpha:1',
            '-alpha|1',
            '-alpha"1"',
            "-alpha'1'",
        )
        for suffix in broken:
            data = {
                'project': 'test',
                'branch': 'master',
                'version': f'0.0.1{suffix}',
            }
            form = forms.UploadForm(data=data)
            self.assertFalse(form.is_valid())


class SystemViewsTestCase(TestCase):

    def setUp(self):
        self.gitlab_id = 'gitlab-server-1'
        self.gitlab_password = get_random_string()
        self.gitlab_api_key = get_random_string(32)
        self.gitlab_token = get_random_string(40)
        conf.SYSTEM_AUTH_BASIC[self.gitlab_id] = self.gitlab_password
        conf.SYSTEM_AUTH_API_KEYS[self.gitlab_id] = self.gitlab_api_key
        conf.SYSTEM_AUTH_API_TOKENS[self.gitlab_token] = self.gitlab_id

    def tearDown(self):
        for v in models.Version.objects.all():
            v.delete()

    def test_system_auth_by_api_key(self):
        """Проверка доступа авторизованного сервера по ключу API."""
        header = f'ApiKey {self.gitlab_id}:{self.gitlab_api_key}'
        browser = Browser(HTTP_AUTHORIZATION=header)
        response = browser.get(reverse('apptrunk:upload'))
        self.assertEqual(response.status_code, 200, response.content.decode())

    def test_system_auth_by_token(self):
        """Проверка доступа авторизованного сервера по токену."""
        header = f'ApiKey {self.gitlab_token}'
        browser = Browser(HTTP_AUTHORIZATION=header)
        response = browser.get(reverse('apptrunk:upload'))
        self.assertEqual(response.status_code, 200, response.content.decode())

    def test_system_auth_by_password(self):
        """Проверка доступа авторизованного сервера по логин-паролю."""
        url = reverse('apptrunk:upload')
        credentials = '%s:%s' % (self.gitlab_id, self.gitlab_password)
        header = f'Basic {b64encode(credentials.encode()).decode()}'
        browser = Browser(HTTP_AUTHORIZATION=header)
        response = browser.get(url)
        self.assertEqual(response.status_code, 200, response.content.decode())

    def test_upload_401(self):
        """Проверка доступа неавторизованного клиента."""
        url = reverse('apptrunk:upload')
        browser = Browser()
        response = browser.get(url)
        self.assertEqual(response.status_code, 401, response.content.decode())

    def test_upload(self):
        """Проверка добавления версии."""
        header = f'ApiKey {self.gitlab_token}'
        browser = Browser(HTTP_AUTHORIZATION=header)
        versions = {
            '0.0.1': (0, 0, 1, '', 'commit-sha1'),
            '0.1': (0, 1, 0, '', 'commit-sha1'),
            '0.1.0': (0, 1, 0, '', 'commit-sha1'),
            '1.0': (1, 0, 0, '', 'commit-sha1'),
            '1.0.0': (1, 0, 0, '', 'commit-sha1'),

            # PEP 440
            '1.0.dev': (1, 0, 0, '.dev', ''),
            '1.0.1.dev': (1, 0, 1, '.dev', ''),
            '1.0.1.dev20201231140000': (1, 0, 1, '.dev20201231140000', ''),
            '1.0a': (1, 0, 0, 'a', ''),
            '1.0a1': (1, 0, 0, 'a1', ''),
            '1.0.1a': (1, 0, 1, 'a', ''),
            '1.0.1a1': (1, 0, 1, 'a1', ''),

            # SemVer 2.0
            '1.0.0~test': (1, 0, 0, '~test', ''),
            '1.0.1~test': (1, 0, 1, '~test', ''),
            '1.0.0-alpha': (1, 0, 0, '-alpha', ''),
            '1.0.0-alpha.1': (1, 0, 0, '-alpha.1', ''),
            '1.0.1-alpha': (1, 0, 1, '-alpha', ''),
            '1.0.1-alpha.1': (1, 0, 1, '-alpha.1', ''),

            '1.0.0+2021-01-01T00:00:00+10:00': (1, 0, 0, '+20201231140000', ''),
            '1.0.0-alpha.2021-01-01T00:00:00+10:00': (1, 0, 0, '-alpha.20201231140000', ''),
            '1.0.0-beta.2021-01-01T00:00:00+10:00': (1, 0, 0, '-beta.20201231140000', ''),
            '1.0.0-rc.2021-01-01T00:00:00+10:00': (1, 0, 0, '-rc.20201231140000', ''),
        }

        for key, value in versions.items():
            with open(conf.__file__, 'rb') as fp:
                response = browser.post(reverse('apptrunk:upload'), {
                    'project': 'test',
                    'branch': 'test',
                    'version': key,
                    'package': fp,
                    # Необязательные поля:
                    'commit': value[4],
                })
            self.assertEqual(response.status_code, 200,
                             f'{key}\n\n{response.content.decode()}')

            version = models.Version.objects.first()
            try:
                self.assertEqual(version.major, value[0], key)
                self.assertEqual(version.minor, value[1], key)
                self.assertEqual(version.patch, value[2], key)
                self.assertEqual(version.suffix, value[3], key)
                self.assertEqual(version.commit, value[4], key)
            finally:
                try:
                    version.delete()
                except Exception:
                    pass


class ClientViewsTestCase(TestCase):

    def setUp(self):
        self.client = models.Client.objects.create(name='client')

    def tearDown(self):
        for v in models.Version.objects.all():
            v.delete()

    def make_browser(self, header=None):
        if not header:
            client = self.client
            header = f'ApiKey {client.id}:{client.api_key}'
        return Browser(HTTP_AUTHORIZATION=header)

    def make_version(self, project='test', branch='test', **fields):
        project, cr = models.Project.objects.get_or_create(name=project)
        branch, cr = models.Branch.objects.get_or_create(project=project,
                                                         name=branch)
        package_file = SimpleUploadedFile(
            name='conf.py',
            content=open(conf.__file__, 'rb').read(),
            content_type='application/x-python',
        )
        version, cr = models.Version.objects.get_or_create(
            project=project, branch=branch, package=package_file, **fields)
        return version

    def test_client_auth_by_api_key(self):
        """Проверка доступа авторизованного клиента по ключу API."""
        browser = self.make_browser()
        response = browser.get(reverse('apptrunk:index'))
        self.assertEqual(response.status_code, 200, response.content.decode())

    def test_client_auth_by_token(self):
        """Проверка доступа авторизованного клиента по токену."""
        client_token = models.ClientToken.objects.create(
            client=self.client, code=get_random_string(),
            expired=now() + timedelta(hours=1),
        )
        header = f'ApiKey {client_token.code}'
        browser = self.make_browser(header)
        response = browser.get(reverse('apptrunk:index'))
        self.assertEqual(response.status_code, 200, response.content.decode())

    def test_client_auth_by_password(self):
        """Проверка доступа авторизованного клиента по логин-паролю."""
        url = reverse('apptrunk:index')
        client_auth = models.ClientAuth.objects.create(
            client=self.client, username='client', password='password')
        credentials = '%s:%s' % (client_auth.username, client_auth.password)
        header = f'Basic {b64encode(credentials.encode()).decode()}'
        browser = self.make_browser(header)
        response = browser.get(url)
        self.assertEqual(response.status_code, 200, response.content.decode())

    def test_index_401(self):
        """Проверка доступа неавторизованного клиента."""
        url = reverse('apptrunk:index')
        browser = Browser()
        response = browser.get(url)
        self.assertEqual(response.status_code, 401, response.content.decode())

    def test_index(self):
        """Проверка выдачи версий клиенту на главной странице."""
        browser = self.make_browser()
        response = browser.get(reverse('apptrunk:index'))
        self.assertEqual(response.status_code, 200, response.content.decode())
        self.assertIn('versions', response.context)
        self.assertIsNotNone(response.context['versions'])

    def test_index_versions(self):
        """Проверка выдачи версий клиенту на главной странице."""
        browser = self.make_browser()
        url = reverse('apptrunk:index')

        v_1_0_0 = self.make_version(major=1)
        v_1_1_0 = self.make_version(major=1, minor=1)
        v_1_2_0 = self.make_version(major=1, minor=2)
        v_2_0_0 = self.make_version(major=2)
        v_3_0_0 = self.make_version(major=3, branch='next')
        restriction = models.Restriction.objects.create(
            project=v_1_0_0.project,
            client=self.client,
        )

        # Все версии проекта.
        response = browser.get(url)
        versions = response.context['versions']
        self.assertEqual(len(versions), 5, 'Restrict by project.')
        self.assertIn(v_3_0_0, versions, 'Restrict by project.')

        # Ограничиваем веткой `test`.
        restriction.branch = v_1_0_0.branch
        restriction.save()
        response = browser.get(url)
        versions = response.context['versions']
        self.assertEqual(len(versions), 4, 'Restrict by branch.')
        self.assertIn(v_2_0_0, versions, 'Restrict by branch.')
        self.assertNotIn(v_3_0_0, versions, 'Restrict by branch.')

        # Ограничиваем первой версией.
        restriction.major_max = 1
        restriction.save()
        response = browser.get(url)
        versions = response.context['versions']
        self.assertEqual(len(versions), 3, 'Restrict by max major.')
        self.assertIn(v_1_2_0, versions, 'Restrict by max major.')
        self.assertNotIn(v_2_0_0, versions, 'Restrict by max major.')
        self.assertNotIn(v_3_0_0, versions, 'Restrict by max major.')

        # Ограничиваем максимальной минорной версией.
        restriction.minor_max = 1
        restriction.save()
        response = browser.get(url)
        versions = response.context['versions']
        self.assertEqual(len(versions), 2, 'Restrict by max minor.')
        self.assertIn(v_1_1_0, versions, 'Restrict by max minor.')
        self.assertNotIn(v_1_2_0, versions, 'Restrict by max minor.')
        self.assertNotIn(v_2_0_0, versions, 'Restrict by max minor.')
        self.assertNotIn(v_3_0_0, versions, 'Restrict by max minor.')

        # Ограничиваем минимальной минорной версией.
        restriction.minor_min = 1
        restriction.save()
        response = browser.get(url)
        versions = response.context['versions']
        self.assertEqual(len(versions), 1, 'Restrict by min minor.')
        self.assertIn(v_1_1_0, versions, 'Restrict by min minor.')
        self.assertNotIn(v_1_0_0, versions, 'Restrict by min minor.')
        self.assertNotIn(v_1_2_0, versions, 'Restrict by min minor.')
        self.assertNotIn(v_2_0_0, versions, 'Restrict by min minor.')
        self.assertNotIn(v_3_0_0, versions, 'Restrict by min minor.')

    def test_check(self):
        """Проверка выдачи последней версии клиенту."""
        browser = self.make_browser()

        v_1_0_0 = self.make_version(major=1)
        v_1_1_0 = self.make_version(major=1, minor=1)
        v_1_2_0 = self.make_version(major=1, minor=2)
        v_2_0_0 = self.make_version(major=2)
        self.make_version(major=2, suffix='other', branch='next')
        self.make_version(major=3, branch='next')
        restriction = models.Restriction.objects.create(
            project=v_1_0_0.project,
            client=self.client,
        )

        url = reverse('apptrunk:check', kwargs={
            'project_name': v_1_0_0.project.name,
            'branch_name': v_1_0_0.branch.name,
        })

        # При запросе ветки `test` не должна попасть версия из ветки `next`.
        response = browser.get(url)
        self.assertEqual(response.status_code, 200, response.content.decode())
        self.assertEqual(response.content.decode(), v_2_0_0.get_version())

        # Ограничиваем веткой `test`.
        restriction.branch = v_1_0_0.branch
        restriction.save()
        response = browser.get(url)
        self.assertEqual(response.content.decode(), v_2_0_0.get_version())

        # Ограничиваем первой версией.
        restriction.major_max = 1
        restriction.save()
        response = browser.get(url)
        self.assertEqual(response.content.decode(), v_1_2_0.get_version())

        # Ограничиваем максимальной минорной версией.
        restriction.minor_max = 1
        restriction.save()
        response = browser.get(url)
        self.assertEqual(response.content.decode(), v_1_1_0.get_version())

        # Ограничиваем минимальной минорной версией.
        restriction.minor_min = 1
        restriction.save()
        response = browser.get(url)
        self.assertEqual(response.content.decode(), v_1_1_0.get_version())

    def test_download(self):
        """Проверка выдачи файла последней версии клиенту."""
        browser = self.make_browser()

        v_1_0_0 = self.make_version(major=1)
        models.Restriction.objects.create(
            project=v_1_0_0.project,
            client=self.client,
        )

        url = reverse('apptrunk:download', kwargs={
            'project_name': v_1_0_0.project.name,
            'branch_name': v_1_0_0.branch.name,
        })

        response = browser.get(url)
        self.assertEqual(response.status_code, 200)
        header = response.headers['Content-Disposition']
        self.assertEqual(header, 'attachment; filename="package.py"')
