#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
import logging
from asgiref.sync import sync_to_async

from django.db.models import F, Q
from django.http import FileResponse, HttpResponse, Http404
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from apptrunk.forms import UploadForm
from apptrunk.models import Version
from apptrunk.decorators import client_access_required, system_access_required

logger = logging.getLogger(__name__)


def get_all_versions(client, project_name, branch_name):
    qs = Version.objects.filter(
        project__name=project_name,
        branch__name=branch_name,
    )
    if client:
        qs = qs.filter(project__restriction__client=client)
        qs = qs.filter(
            Q(project__restriction__branch__isnull=True) |
            Q(branch=F('project__restriction__branch')),
            Q(project__restriction__major_min__isnull=True) |
            Q(major__gte=F('project__restriction__major_min')),
            Q(project__restriction__minor_min__isnull=True) |
            Q(minor__gte=F('project__restriction__minor_min')),
            Q(project__restriction__major_max__isnull=True) |
            Q(major__lte=F('project__restriction__major_max')),
            Q(project__restriction__minor_max__isnull=True) |
            Q(minor__lte=F('project__restriction__minor_max')),
        )
    qs = qs.order_by('-major', '-minor', '-patch', '-is_release', '-created')
    return qs


def get_last_version(client, project_name, branch_name):
    qs = get_all_versions(client, project_name, branch_name)
    return qs.first()


@sync_to_async
@client_access_required
def index(request):

    if request.client:
        client_sql = """
    JOIN apptrunk_restriction ON (
        apptrunk_restriction.project_id = apptrunk_project.id
        AND (
            apptrunk_restriction.branch_id IS NULL
            OR
            apptrunk_restriction.branch_id = apptrunk_branch.id
        )
    )
    WHERE
        apptrunk_restriction.client_id = %s
        AND (
            apptrunk_restriction.major_min IS NULL
            OR
            apptrunk_version.major >= apptrunk_restriction.major_min
        )
        AND (
            apptrunk_restriction.minor_min IS NULL
            OR
            apptrunk_version.minor >= apptrunk_restriction.minor_min
        )
        AND (
            apptrunk_restriction.major_max IS NULL
            OR
            apptrunk_version.major <= apptrunk_restriction.major_max
        )
        AND (
            apptrunk_restriction.minor_max IS NULL
            OR
            apptrunk_version.minor <= apptrunk_restriction.minor_max
        )
        """
        params = [request.client.id]
    else:
        client_sql = ""
        params = []

    sql = f"""
SELECT *
FROM (
    SELECT
        apptrunk_version.*,
        apptrunk_project.name AS project_name,
        apptrunk_branch.name AS branch_name,
        RANK() OVER(
            PARTITION BY
                apptrunk_version.project_id,
                apptrunk_version.branch_id
            ORDER BY
                apptrunk_version.major DESC,
                apptrunk_version.minor DESC,
                apptrunk_version.patch DESC,
                apptrunk_version.is_release DESC,
                apptrunk_version.created DESC
        ) AS num
    FROM apptrunk_version
    JOIN apptrunk_project ON (apptrunk_project.id = apptrunk_version.project_id)
    JOIN apptrunk_branch ON (apptrunk_branch.id = apptrunk_version.branch_id)
    {client_sql}
) AS T0
WHERE
    num <= 10
ORDER BY
    project_name,
    branch_name,
    major DESC,
    minor DESC,
    patch DESC,
    is_release DESC,
    created DESC
    """

    qs = Version.objects.raw(sql, params)
    ctx = {'versions': qs}
    return render(request, 'apptrunk/index.html', ctx)


@sync_to_async
@client_access_required
def versions(request, project_name, branch_name):
    qs = get_all_versions(request.client, project_name, branch_name)
    ctx = {
        'versions': qs,
        'project_name': project_name,
        'branch_name': branch_name,
    }
    return render(request, 'apptrunk/versions.html', ctx)


@sync_to_async
@csrf_exempt
@system_access_required
@require_http_methods(['GET', 'POST'])
def upload(request):
    if request.method == 'GET':
        return HttpResponse('OK', content_type="text/plain")
    form = UploadForm(request.POST, request.FILES)
    if form.is_valid():
        form.save()
        return HttpResponse('OK', content_type="text/plain")
    lines = ['ERROR']
    for key, val in form.errors.items():
        if key != '__all__':
            lines.append('%s:' % key)
            tab = '\t'
        else:
            tab = ''
        for e in val:
            lines.append('%s%s' % (tab, e))
    lines.append('')
    return HttpResponse('\n'.join(lines), status=400, content_type="text/plain")


@sync_to_async
@client_access_required
def check(request, project_name, branch_name):
    version = get_last_version(request.client, project_name, branch_name)
    if not version:
        raise Http404
    return HttpResponse(version.get_version(), content_type="text/plain")


@sync_to_async
@client_access_required(or_system=True)
def download(request, project_name, branch_name):
    version = get_last_version(request.client, project_name, branch_name)
    if not version and 'or' in request.GET:
        branch_name = request.GET['or']
        version = get_last_version(request.client, project_name, branch_name)
    if not version:
        raise Http404
    filename, open_file = version.get_download_info()
    return FileResponse(open_file, as_attachment=True, filename=filename)
