#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django import forms
from django.core.exceptions import ValidationError
from django.db import transaction
from django.utils.dateparse import parse_datetime

from django.utils.timezone import utc, localtime
from django.utils.translation import gettext_lazy as _

from apptrunk.models import Project, Branch, Version
from apptrunk.utils import autoclean
from apptrunk.validators import validate_version, validate_suffix


class UploadForm(forms.ModelForm):
    """
    Форма для загрузки версии.
    """
    project = forms.CharField(max_length=50)
    branch = forms.CharField(max_length=50)
    version = forms.CharField(validators=[validate_version])
    semver = forms.CharField(required=False)
    timestamp = forms.CharField(required=False)

    class Meta:
        model = Version
        fields = (
            # 'major',
            # 'minor',
            # 'patch',
            # 'suffix',
            # 'semver',
            'package',
            'commit',
        )

    def clean_version(self):
        value = self.cleaned_data['version']
        groups = validate_version.regex.match(value).groups()
        major, minor, dot, patch, suffix = groups
        if minor is None:
            minor = 0
        if patch is None:
            patch = 0
        if suffix is None:
            suffix = ''

        # Исправление обрезки первичной точки в суффиксах `.dev*`.
        if suffix.startswith('dev'):
            suffix = f'.{suffix}'

        # Обработка передачи CI_COMMIT_TIMESTAMP в суффиксе.
        # Формат +2021-01-01T00:00:00+10:00 преобразовываем
        # в UTC и в число.
        if suffix.startswith('+'):
            datetime = parse_datetime(suffix[1:])
            if datetime:
                datetime = localtime(datetime, timezone=utc)
                suffix = '+%s' % datetime.strftime('%Y%m%d%H%M%S')
        # Формат -[alpha,beta,rc].2021-01-01T00:00:00+10:00 преобразовываем
        # в UTC и в число.
        elif '.' in suffix[1:]:
            head, tail = suffix.split('.', 1)
            datetime = parse_datetime(tail)
            if datetime:
                datetime = localtime(datetime, timezone=utc)
                suffix = '%s.%s' % (head, datetime.strftime('%Y%m%d%H%M%S'))

        if suffix:
            validate_suffix(suffix)

        return int(major), int(minor), int(patch or 0), suffix

    def clean_semver(self):
        return self.cleaned_data['semver'] not in ('false', 'no')

    def clean_timestamp(self):
        value = self.cleaned_data['timestamp'] or ''
        if value:
            datetime = parse_datetime(value)
            if datetime:
                datetime = localtime(datetime, timezone=utc)
                value = '+%s' % datetime.strftime('%Y%m%d%H%M%S')
            else:
                value = ''
        return value

    def clean(self):
        cdata = super().clean()
        project = cdata.get('project')
        branch = cdata.get('branch')
        version = cdata.get('version')
        if project and branch and version:
            ma, mi, pa, su = version
            # Суффикс, начинающийся на ".dev", обычно уже содержит время коммита.
            if su == '.dev' or not su.startswith('.dev'):
                su += cdata.get('timestamp', '')
            qs = Version.objects.filter(
                project__name=project,
                branch__name=branch,
                major=ma,
                minor=mi,
                patch=pa,
                suffix=su,
            )
            if qs.exists():
                v = '%s (%s): %d.%d.%d%s' % (
                    project,
                    branch,
                    ma,
                    mi,
                    pa,
                    su,
                )
                raise ValidationError(
                    _('Версия %r уже есть в хранилище.') % v
                )

    def save(self, **kwargs):
        instance = super().save(commit=False)
        cdata = self.cleaned_data

        ma, mi, pa, su = cdata['version']
        # Суффикс, начинающийся на ".dev", обычно уже содержит время коммита.
        if su == '.dev' or not su.startswith('.dev'):
            su += cdata.get('timestamp', '')
        instance.major = ma
        instance.minor = mi
        instance.patch = pa
        instance.suffix = su
        semver = cdata['semver']
        if semver and su and su[0] not in ('-', '+'):
            semver = False
        instance.semver = semver

        project, project_cr = Project.objects.get_or_create(
            name=cdata['project'],
        )
        branch, branch_cr = Branch.objects.get_or_create(
            project=project,
            name=cdata['branch'],
        )
        with transaction.atomic():
            instance.project = project
            instance.branch = branch
            instance.save()
            project.updated = instance.updated
            project.save(update_fields=['updated'])
            branch.updated = instance.updated
            branch.save(update_fields=['updated'])
            autoclean(instance)
        return instance
