#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
import os
import sys
from djangokit.utils import version

VERSION = (0, 3, 1, 'final', 0)


def get_version():
    path = os.path.dirname(os.path.abspath(__file__))
    return version.get_version(VERSION, path)


__version__ = get_version()


TESTING = bool(
    sys.argv and
    ('test' in sys.argv or os.path.basename(sys.argv[0]).startswith('runtests.'))
)
