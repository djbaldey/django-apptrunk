#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.conf import settings
from django.db import models
# from django.utils.crypto import get_random_string
# from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from djangokit.utils.strings import make_unique_secret

from apptrunk.utils import upload_package, get_ext
from apptrunk.validators import validate_suffix


class Client(models.Model):
    """
    Модель клиента, с его описанием и API-ключом авторизации.
    """
    created = models.DateTimeField(_('создан'), auto_now_add=True)
    updated = models.DateTimeField(_('обновлён'), auto_now=True)
    is_active = models.BooleanField(
        _('активен'), default=True, db_index=True, help_text=_(
            'Отключите, если клиент больше не имеет доступа к системе.'
        )
    )
    name = models.CharField(_('название'), max_length=100)
    # Выдача токенов осуществляется при совместном указании `id` и `api_key`.
    # Так же вариантом авторизации может выступать этот ключ API совместно с
    # ID клиента так: `Authorization ApiKey=id:api_key`.
    api_key = models.CharField(
        _('ключ для выдачи токенов'), max_length=40,
        default=make_unique_secret)
    description = models.TextField(_('описание'), blank=True)

    # Пользователь, который последним изменил объект.
    last_editor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True, blank=True,
        editable=False,
        verbose_name=_('последний редактор'),
        on_delete=models.PROTECT,
    )

    class Meta:
        ordering = ('name',)
        verbose_name = _('клиент')
        verbose_name_plural = _('клиенты')

    def __str__(self):
        return self.name


class ClientAuth(models.Model):
    """
    Модель данных для HTTP-аутентификации клиентов.
    """
    created = models.DateTimeField(_('создана'), auto_now_add=True)
    updated = models.DateTimeField(_('обновлена'), auto_now=True)
    username = models.CharField(
        _('имя пользователя'), max_length=20, unique=True)
    password = models.CharField(_('пароль'), max_length=20)
    client = models.ForeignKey(
        Client, on_delete=models.CASCADE,
        verbose_name=_('клиент'))

    class Meta:
        ordering = ('username',)
        verbose_name = _('аутентификация клиента')
        verbose_name_plural = _('аутентификации клиентов')

    def __str__(self):
        return self.username


class ClientToken(models.Model):
    """
    Модель данных для авторизации по токенам.
    """
    created = models.DateTimeField(
        _('создан'), auto_now_add=True, db_index=True)
    expired = models.DateTimeField(_('истекает'), db_index=True)
    client = models.ForeignKey(
        Client, on_delete=models.CASCADE,
        verbose_name=_('клиент'))
    code = models.CharField(_('код токена'), max_length=256, unique=True)

    class Meta:
        ordering = ('-created',)
        verbose_name = _('токен авторизации')
        verbose_name_plural = _('токены авторизации')

    def __str__(self):
        code = self.code
        return '%s...%s' % (code[:6], code[-6:])


class Project(models.Model):
    """
    Модель проекта.
    """
    created = models.DateTimeField(_('создан'), auto_now_add=True, db_index=True)
    updated = models.DateTimeField(_('обновлён'), auto_now=True, db_index=True)

    name = models.CharField(
        _('название'), max_length=50, unique=True,
        help_text=_('Обязательно. 50 символов и менее.'),
        error_messages={
            'unique': _('Такое название уже существует.'),
        },
    )
    cleanup = models.PositiveSmallIntegerField(
        _('очистка для новых веток'), null=True, blank=True, help_text=_(
            'Это значение означает, что все новые ветки будут созданы с '
            'этим значением очистки. Так же это значение применится для '
            'всех существующих веток с пустым значением очистки.'
        )
    )

    class Meta:
        ordering = ('name',)
        verbose_name = _('проект')
        verbose_name_plural = _('проекты')

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        super().save(**kwargs)
        if self.cleanup:
            # Нулевое значение должно быть исключено, т.к. служит для
            # жёсткой фиксации запрета чистки.
            qs = self.branch_set.exclude(cleanup__isnull=False)
            qs.update(cleanup=self.cleanup)


class Branch(models.Model):
    """
    Модель ветки проекта.
    """
    created = models.DateTimeField(_('создана'), auto_now_add=True, db_index=True)
    updated = models.DateTimeField(_('обновлена'), auto_now=True, db_index=True)
    project = models.ForeignKey(
        Project,
        # editable=False,
        verbose_name=_('проект'),
        on_delete=models.PROTECT,
    )
    name = models.CharField(
        _('название'), max_length=100,
        help_text=_('Обязательно. 100 символов и менее.'),
    )
    cleanup = models.PositiveSmallIntegerField(
        _('подчищать'), null=True, blank=True, help_text=_(
            'Установите значение оставляемых после автоматической чистки '
            'минорных версий. Пустое и нулевое значения отключают такую чистку.'
        )
    )

    class Meta:
        ordering = ('name',)
        unique_together = ('project', 'name')
        verbose_name = _('ветка проекта')
        verbose_name_plural = _('ветки проектов')

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        if not self.pk:
            self.cleanup = self.project.cleanup
        super().save(**kwargs)


class Version(models.Model):
    """
    Модель версии проекта.
    """
    created = models.DateTimeField(_('создана'), auto_now_add=True, db_index=True)
    updated = models.DateTimeField(_('обновлена'), auto_now=True, db_index=True)
    project = models.ForeignKey(
        Project,
        # editable=False,
        verbose_name=_('проект'),
        on_delete=models.PROTECT,
    )
    branch = models.ForeignKey(
        Branch,
        # editable=False,
        verbose_name=_('ветка'),
        on_delete=models.PROTECT,
    )
    major = models.PositiveSmallIntegerField(
        _('мажорная версия'), default=0, db_index=True,
    )
    minor = models.PositiveSmallIntegerField(
        _('минорная версия'), default=0, db_index=True,
    )
    patch = models.PositiveSmallIntegerField(
        _('патч-версия'), default=0, db_index=True,
    )
    suffix = models.CharField(
        _('предрелизная версия'), max_length=50, blank=True,
        validators=[validate_suffix],
    )
    is_release = models.BooleanField(
        _('это релиз'), default=True, db_index=True,
        editable=False,
    )
    semver = models.BooleanField(_('семантическая версия'), default=True)
    package = models.FileField(
        _('файл пакета'), upload_to=upload_package,
    )
    package_sha1 = models.CharField(
        _('контрольная сумма SHA1'), max_length=40, blank=True,
    )
    commit = models.CharField(
        _('коммит'), max_length=40, blank=True,
    )

    class Meta:
        ordering = ('-updated',)
        unique_together = (
            'project', 'branch', 'major', 'minor', 'patch', 'suffix')
        verbose_name = _('версия проекта')
        verbose_name_plural = _('версии проектов')

    def __str__(self):
        return self.get_version()

    @property
    def short_commit(self):
        return self.commit[:7]

    def get_version(self):
        if self.suffix and (self.semver or self.patch):
            tmp = '%(major)s.%(minor)s.%(patch)s%(suffix)s'
        elif self.suffix:
            tmp = '%(major)s.%(minor)s%(suffix)s'
        elif self.patch:
            tmp = '%(major)s.%(minor)s.%(patch)s'
        else:
            tmp = '%(major)s.%(minor)s'
        return tmp % {
            'major': self.major,
            'minor': self.minor,
            'patch': self.patch,
            'suffix': self.suffix,
        }

    def get_full_name(self):
        tmp = '%(project)s-%(version)s~%(branch)s'
        return tmp % {
            'project': self.project.name,
            'branch': self.branch.name,
            'version': self.get_version(),
        }

    def get_download_info(self):
        path = self.package.path
        ext = get_ext(path)
        return 'package%s' % ext, open(path, 'rb')

    def save(self, **kwargs):
        if self.semver and not self.major:
            self.is_release = False
        elif self.semver and self.suffix.startswith('+'):
            self.is_release = True
        elif self.suffix:
            self.is_release = False
        else:
            self.is_release = True
        super().save(**kwargs)
        self.project.save(update_fields=['updated'])
        self.branch.save(update_fields=['updated'])

    def delete(self, **kwargs):
        self.package.delete()
        super().delete(**kwargs)


class Restriction(models.Model):
    """
    Модель ограничения доступа клиента к версиям проекта.
    """
    created = models.DateTimeField(_('создано'), auto_now_add=True)
    updated = models.DateTimeField(_('обновлено'), auto_now=True, db_index=True)
    client = models.ForeignKey(
        Client,
        verbose_name=_('клиент'),
        on_delete=models.CASCADE,
    )
    project = models.ForeignKey(
        Project,
        verbose_name=_('проект'),
        on_delete=models.CASCADE,
    )
    # Со значением NULL имеет доступ ко всем веткам.
    branch = models.ForeignKey(
        Branch,
        blank=True, null=True,
        verbose_name=_('ветка'),
        on_delete=models.CASCADE,
    )
    major_min = models.PositiveSmallIntegerField(
        _('минимальная мажорная версия'), blank=True, null=True, db_index=True,
    )
    # Минорные ограничения должны заполнятся только при соответственно
    # заполненных мажорных ограничений. Смотрите метод save().
    minor_min = models.PositiveSmallIntegerField(
        _('минимальная минорная версия'), blank=True, null=True, db_index=True,
    )
    major_max = models.PositiveSmallIntegerField(
        _('максимальная мажорная версия'), blank=True, null=True, db_index=True,
    )
    minor_max = models.PositiveSmallIntegerField(
        _('максимальная минорная версия'), blank=True, null=True, db_index=True,
    )

    # Пользователь, который последним изменил объект.
    last_editor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True, blank=True,
        editable=False,
        verbose_name=_('последний редактор'),
        on_delete=models.PROTECT,
    )

    class Meta:
        ordering = ('-updated',)
        verbose_name = _('ограничение')
        verbose_name_plural = _('ограничения')

    def __str__(self):
        s = '%s:%s:%s' % (self.client, self.project, self.branch or 'all')
        min_version = self.get_min_version()
        max_version = self.get_max_version()
        if min_version:
            s = '%s >=%s' % (s, min_version)
        if max_version:
            s = '%s <=%s' % (s, max_version)
        return s

    def get_min_version(self):
        if self.minor_min is not None:
            return '%d.%d' % (self.major_min or 0, self.minor_min)
        elif self.major_min is not None:
            return '%d.0' % self.major_min

    def get_max_version(self):
        if self.minor_max is not None:
            return '%d.%d' % (self.major_max or 0, self.minor_max)
        elif self.major_max is not None:
            return '%d.32767' % self.major_max

    def save(self, **kwargs):
        if self.minor_min is not None and self.major_min is None:
            self.major_min = 0
        if self.minor_max is not None and self.major_max is None:
            self.major_max = 0
        if (self.major_min or 0) > 32766:
            self.major_min = 32766
        if (self.minor_min or 0) > 32766:
            self.minor_min = 32766
        if (self.major_max or 0) > 32767:
            self.major_max = 32767
        if (self.minor_max or 0) > 32767:
            self.minor_max = 32767
        if self.major_min is not None \
                and self.minor_max \
                and self.major_min > (self.major_max or 0):
            self.major_max = self.major_min
        if self.major_min is not None \
                and self.major_min == (self.major_max or 0) \
                and self.minor_min is not None \
                and self.minor_max is not None \
                and self.minor_min >= self.minor_max:
            self.minor_max = self.minor_min + 1

        super().save(**kwargs)
