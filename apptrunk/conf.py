#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.conf import settings

PACKAGE_UPLOAD_PATH = getattr(
    settings,
    'PACKAGE_UPLOAD_PATH',
    'packages/%(project)s/%(branch)s/%(version)s%(ext)s')

# example: {'username': 'password'}
SYSTEM_AUTH_BASIC = getattr(settings, 'SYSTEM_AUTH_BASIC', {})
# example: {'id-of-the-server': 'api-key'}
SYSTEM_AUTH_API_KEYS = getattr(settings, 'SYSTEM_AUTH_API_KEYS', {})
# example: {'token', 'name-or-id-of-the-server'}
SYSTEM_AUTH_API_TOKENS = getattr(settings, 'SYSTEM_AUTH_API_TOKENS', {})
