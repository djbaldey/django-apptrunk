#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from os.path import splitext
from unidecode import unidecode

# from django.utils.timezone import now

from apptrunk.conf import PACKAGE_UPLOAD_PATH


def get_ext(path):
    name, ext = splitext(path)
    name2, ext2 = splitext(name)
    if ext2 == '.tar':
        ext = ext2 + ext
    return ext


def upload_package(instance, filename):
    filename = unidecode(filename).lower()
    filename = filename.replace(' ', '_').replace("'", '').replace('"', '')
    name, ext = splitext(filename)
    name2, ext2 = splitext(name)
    if ext2 == '.tar':
        ext = ext2 + ext
        name = name2
    dic = {
        'project': instance.project.name,
        'branch': instance.branch.name,
        'version': instance.get_version(),
        'commit': instance.commit or 'no-commit',
        'short_commit': instance.short_commit,
        'sha1': instance.package_sha1 or 'no-sha1',
        'filename': filename,
        'name': name,
        'ext': ext,
        'date': instance.created.date().isoformat(),
    }
    return PACKAGE_UPLOAD_PATH % dic


def autoclean(version):
    branch = version.branch
    cleanup = branch.cleanup
    if cleanup:
        qs = version.__class__.objects.filter(
            branch=branch,
            major=version.major,
            minor=version.minor,
        )
        qs = qs.order_by('-patch', '-is_release', '-created')
        for v in qs[cleanup:]:
            v.delete()
