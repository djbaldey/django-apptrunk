#
# Copyright (c) 2022, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.core.validators import RegexValidator
from django.utils.regex_helper import _lazy_re_compile
from django.utils.translation import gettext_lazy as _


validate_version = RegexValidator(
    _lazy_re_compile(r'^(\d+)\.(\d+)(\.)?(\d+)?(.*)$'),
    _('Укажите правильную версию.'),
    'invalid',
)


validate_suffix = RegexValidator(
    _lazy_re_compile(r"^[-a-zA-Z0-9_\.\+\~]+\Z"),
    _('Укажите правильный суффикс.'),
    'invalid',
)
