#
# Copyright (c) 2021, Grigoriy Kramarenko <root@rosix.ru>
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from apptrunk.models import (
    Client, ClientAuth, ClientToken, Project, Branch, Version, Restriction,
)

register = admin.site.register


class ClientAdmin(admin.ModelAdmin):
    list_select_related = ('last_editor',)
    list_display = ('name', 'is_active', 'description', 'short_api_key',
                    'updated', 'last_editor', 'id')
    date_hierarchy = 'updated'
    list_filter = ('is_active', 'created', 'updated')

    def short_api_key(self, obj):
        return '%s...%s' % (obj.api_key[:6], obj.api_key[-6:])
    short_api_key.short_description = _('ключ для токенов')


class ClientAuthAdmin(admin.ModelAdmin):
    list_select_related = ('client',)
    list_display = ('username', 'client', 'created', 'updated', 'id')
    date_hierarchy = 'updated'
    list_filter = ('created', 'updated')


class ClientTokenAdmin(admin.ModelAdmin):
    list_select_related = ('client',)
    list_display = ('__str__', 'client', 'created', 'expired', 'id')
    date_hierarchy = 'created'
    list_filter = ('created', 'expired')


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'created', 'updated', 'cleanup', 'id')
    date_hierarchy = 'updated'
    list_filter = ('created', 'updated')


class BranchAdmin(admin.ModelAdmin):
    list_select_related = ('project',)
    list_display = ('__str__', 'project', 'created', 'updated', 'cleanup', 'id')
    date_hierarchy = 'updated'
    list_filter = ('created', 'updated', 'project')


class BranchListFilter(admin.SimpleListFilter):
    title = _('ветка')
    parameter_name = 'branch_name'

    def lookups(self, request, model_admin):
        qs = Branch.objects.all()
        query = request.GET
        project = query.get('project__id__exact')
        # branch = query.get('branch_name')
        if project:
            qs = qs.filter(project_id=project)
        # elif branch:
        #     qs = qs.filter(name=branch)
        # else:
        #     qs = qs.none()
        qs = qs.order_by('name')
        names = qs.values_list('name', flat=True).distinct()
        return ((name, name) for name in names)

    def queryset(self, request, queryset):
        value = self.value()
        if value:
            return queryset.filter(branch__name=value)
        # return queryset


class VersionAdmin(admin.ModelAdmin):
    list_select_related = ('project', 'branch')
    list_display = ('__str__', 'project', 'branch', 'is_release', 'created',
                    'updated', 'id')
    date_hierarchy = 'updated'
    list_filter = ('created', 'updated', 'project', BranchListFilter)

    def delete_queryset(self, request, queryset):
        for obj in queryset:
            obj.delete()


class RestrictionAdmin(admin.ModelAdmin):
    list_select_related = ('client', 'project', 'branch', 'last_editor')
    list_display = ('__str__', 'created', 'updated', 'id')
    date_hierarchy = 'updated'
    list_filter = ('created', 'updated')


register(Client, ClientAdmin)
register(ClientAuth, ClientAuthAdmin)
register(ClientToken, ClientTokenAdmin)
register(Project, ProjectAdmin)
register(Branch, BranchAdmin)
register(Version, VersionAdmin)
register(Restriction, RestrictionAdmin)
